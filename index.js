const express = require('express')
const helmet = require('helmet')
var bodyParser = require('body-parser');
const app = express()

// add some security-related headers to the response
app.use(helmet())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.get('/', (req, res) => {
    // res.set('Content-Type', 'text/html')
    res.send(200, `
        <h1><marquee direction=right>Hello from Express path '/' on Now 2.0!</marquee></h1>
        <h2>Go to <a href="/about">/about</a></h2>
    `)
})
app.post('/newuser', function (req, res) {
    var json = req.body;
    res.send('Add new ' + json.name + ' Completed!');
});


module.exports = app
